#!/usr/bin/env bash

DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

LINKDEST_NOLINK_DEFAULT="%nolink%"
LINKDEST="${1:-"${LINKDEST_NOLINK_DEFAULT}"}"

TORRFILES_DIR_XDG="$(xdg-user-dir DOWNLOAD 2> /dev/null)"
TORRFILES_DIR_PREFIX="${TORRFILES_DIR_XDG:-"${HOME}/Downloads"}"
TORRFILES_DIR_DEFAULT="${TORRFILES_DIR_PREFIX}/torrents"

BINDIR="${HOME}/.local/bin"


if [[ "${LINKDEST}" == "${LINKDEST_NOLINK_DEFAULT}" ]]
then  mkdir -p "${TORRFILES_DIR_DEFAULT}"
else  ln -sfnr  "${LINKDEST}"  "${TORRFILES_DIR_DEFAULT}"
fi


chmod +x "${DIR}/magnet-get"
mkdir -p "${BINDIR}"
ln -s -f -n "${DIR}/magnet-get"  "${BINDIR}/magnet-get"


MIMES_DIR="${HOME}/.local/share/applications"
mkdir -p "${MIMES_DIR}"

ln -sfn "${DIR}/magnet-local.desktop"  "${MIMES_DIR}/magnet-local.desktop"

read -p 'Press 1 to use magnet-remote.desktop as default mimetype handler, anything else for magnet-local.desktop > ' HANDLERCHOICE
if [[ "${HANDLERCHOICE}" == 1 ]]; then
    "${DIR}/magnet-remote-desktop-generate.sh"
    xdg-mime default magnet-remote.desktop x-scheme-handler/magnet
else
    xdg-mime default magnet-local.desktop x-scheme-handler/magnet
fi
