#!/usr/bin/env bash

DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" && pwd)"

MIMES_DIR="${HOME}/.local/share/applications"

read -p 'Which host you want the metafile created from magnet links be sent to? (prepend .ssh/config aliases with a %-sign) > ' MAGNETGET_HOST_RAW
MAGNETGET_HOST="${MAGNETGET_HOST_RAW/#%/%%}"


cat <<EOF > "${MIMES_DIR}/magnet-remote.desktop"
[Desktop Entry]
Name=magnet-remote
Type=Application
Comment=Open a magnet link with a script and put it somewhere remote
Exec=magnet-get %U ${MAGNETGET_HOST}
Categories=Network;WebBrowser;
MimeType=x-scheme-handler/magnet;
EOF

