#!/usr/bin/env bash

DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

RTORRENTRC_PATH="rtorrent-config/rtorrent.rc"
RTORRENTRC_FILE="${DIR}/${RTORRENTRC_PATH}"
RTORRENTRC_CFG_D="${DIR}/rtorrent-config/config.d"

TORR_ROOT_XDG="$(xdg-user-dir DOWNLOAD 2> /dev/null)"
TORR_ROOT_PREFIX="${TORR_ROOT_XDG:-"${HOME}/Downloads"}"
TORR_ROOT_DEFAULT="${TORR_ROOT_PREFIX}/rtorrent"
TORR_ROOT="$(readlink -f "${1:-"${TORR_ROOT_DEFAULT}"}")"


sudo apt install rtorrent


sed -i "1s/~\/Downloads\/rtorrent/${TORR_ROOT//\//\\/}/"  "${RTORRENTRC_FILE}"
[[ -e "${DIR}/../.git/modules/env.rtorrentrc/info/exclude" ]]  &&  echo "${RTORRENTRC_PATH}"  >> "${DIR}/../.git/modules/env.rtorrentrc/info/exclude"
[[ -e "${DIR}/.git/info/exclude" ]]                            &&  echo "${RTORRENTRC_PATH}"  >> "${DIR}/.git/info/exclude"
git update-index --skip-worktree "${RTORRENTRC_PATH}"


mkdir -p "${XDG_CONFIG_HOME:-"${HOME}/.config"}/rtorrent"
ln -sfnr  "${RTORRENTRC_FILE}"  "${XDG_CONFIG_HOME:-"${HOME}/.config"}/rtorrent/rtorrent.rc"

mkdir -p  "${TORR_ROOT}/config.d"/{global,local}
cp "${RTORRENTRC_CFG_D}/local"/* "${TORR_ROOT}/config.d/local"/
for cfg in "${RTORRENTRC_CFG_D}/global"/*; do
    ln -sfn  "${cfg}"  "${TORR_ROOT}/config.d/global/$(basename "${cfg}")"
done


"${DIR}/magnet-get/install.sh"  "${TORR_ROOT}/watch/start"

